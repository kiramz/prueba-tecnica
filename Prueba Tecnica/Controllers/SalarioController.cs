﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Prueba_Tecnica.Models;
using Prueba_Tecnica.Services;

namespace Prueba_Tecnica.Controllers
{
    public class SalarioController : Controller
    {
        //Declarar el servicio
        private SalarioService salarioService;


        //Constructor del controlador
        public SalarioController()
        {
            //Inicializar el servicio
            this.salarioService = new SalarioService();
        }

        // GET: Salario
        // Pagina principal del controlador
        // @Return ActionResult: retorna una vista @Index con una lista de objetos de tipo SALARIO
        public ActionResult Index()
        {
            return View(this.salarioService.ObtenerTodos());
        }


        // GET: Salario/Create
        // Redirecciona a la vista para crear objetos de tipo SALARIO
        // @Return ActionResult: retorna la vista @Create
        public ActionResult Create()
        {
            return View();
        }



        // POST: Salario/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        // Realiza la acción de creación de un objeto de tipo SALARIO
        // @Param SALARIO salario: objeto de tipo SALARIO que incluye del formulario únicamente los campos "EMPLEADO,SALARIO_BRUTO,PENSION,EMBARGO1,EMBARGO2"
        // @Return ActionResult: retorna a la vista @Index
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EMPLEADO,SALARIO_BRUTO,PENSION,EMBARGO1,EMBARGO2")] SALARIO salario)
        {
            if (ModelState.IsValid)
            {
               
                this.salarioService.Create(salario);
                return RedirectToAction("Index");
            }

            return View(salario);
        }



        // GET: Salario/Delete/5
        // Redirecciona a la vista de eliminación de empleados, busca el registro mediante el id.
        // @Param int id, default 0: corresponde al id del objeto de tipo SALARIO a eliminar, el valor por defecto es 0 y devuelve un badrequest dado que no es valido
        // @Return ActionResult: retorna a la vista @Delete
        public ActionResult Delete(int id = 0)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           
            SALARIO salario = this.salarioService.ObtenerPorId(id);
            if (salario == null)
            {
                return HttpNotFound();
            }
            return View(salario);
        }

        // POST: Salario/Delete/5
        // Realiza la acción de eliminar un objeto de tipo SALARIO
        // @Param int id: corresponde al id del objeto de tipo SALARIO a eliminar y devuelve un badrequest dado que no es valido
        // @Return ActionResult: retorna a la vista @Index
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            
            this.salarioService.Delete(id);
            return RedirectToAction("Index");
        }
    }
}
