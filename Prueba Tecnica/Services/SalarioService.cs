﻿using Prueba_Tecnica.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Prueba_Tecnica.Services
{

    // Servicio para la gestión de consultas correspondientes al salario, de ser necesario se pueden implementar interfaces y repositorios.
    public class SalarioService: IDisposable
    {
        /*Contexto de conexión con la base de datos
        * Models/Salario.Context.tt/Salario.Context.cs
        * La conexión con la base de datos está definida en el Web.config
        */
        private PruebaDB db = new PruebaDB();

        //Constantes

        //Salario inembargable - definido en el documento como el menor salario mensual establecido por ley
        const decimal SALARIO_INEMBARGABLE = 54995.85M;

        //Cargos sociales - definido por el 9% del salario bruto
        const decimal CARGAS_SOCIALES = 0.09M;


        
        /* Permite listar toda la información de la tabla y procesar la información
        * @Return List<SALARIO> : Lista de objetos de tipo SALARIO
        */
        public List<SALARIO> ObtenerTodos()
        {
            List<SALARIO> datos = db.SALARIO.ToList();

            //Procesa los objetos SALARIO al cargar la página, se realiza de esta manera dado que es una prueba técnica pero debería ser optimizado.
            foreach (SALARIO empleado in datos)
            {
                ProcesarEmbargos(empleado);
            }
            return db.SALARIO.ToList();
        }

        /* Permite crear un objeto de tipo SALARIO y guardarlo en la base de datos
         * @Param SALARIO salario: corresponde a un objeto de tipo SALARIO enviado desde el controlador
         * @Return int : returna un número dependiendo de los resultados de la acción de guardado en la base de datos 
         */
        public int Create(SALARIO salario)
        {
            // Asigno los valores por defecto dado que no los definí en la base de datos, ni en el modelo y el usuario no los ingresa
            salario.MONTO_DEDUCIBLE = 0;
            salario.REBAJO_EMBARGO = 0;
            salario.REBAJO_PENSION = 0;
            db.SALARIO.Add(salario);
            return db.SaveChanges();
        }


        /* Obtener un objeto de tipo SALARIO mediante la busqueda por ID
         * @Param int id: corresponde al id con el que buscaremos un registro en la base de datos
         * @Return SALARIO: retorna un objeto de tipo SALARIO o null si no lo encuetra
         */
        public SALARIO ObtenerPorId(int id)
        {
            return db.SALARIO.Find(id);
        }

       /* Permite eliminar un registro de la base de datos
        * @Param int id: corresponde al id con el que buscaremos un registro en la base de datos
        */
        public void Delete(int id)
        {
            SALARIO salarioEmpleado = db.SALARIO.Find(id);
            db.SALARIO.Remove(salarioEmpleado);
            db.SaveChanges();
        }

        /* Permite realizar el calculo del salario neto del empleado
         * Salario neto es igual al salario bruto menos el nueve porciento de cargos sociales
         * @Param decimal salarioBruto: se require el salario bruto del empleado para obtener el salario neto
         * @Return decimal: retorna un valor decimal correspondiente al salario neto
         */
        private decimal CalcularSalarioNeto(decimal salarioBruto)
        {
            return salarioBruto - (salarioBruto * CARGAS_SOCIALES);
        }


        /* Permite realizar el calculo de la suma a embargar del salario o el monto deducible del mismo
         * Salarios entre el valor de la constante SALARIO_INEMBARGABLE y tres veces la misma son embargables en una octaba parte
         * Salarios que sobre pasen tres veces el valor de la constante SALARIO_INEMBARGABLE son embargables en una cuarta parte
         * @Param decimal salarioBruto: se require el salario bruto del empleado para obtener el salario neto
         * @Return decimal: retorna un valor decimal correspondiente al salario neto
         */
        private decimal CalcularSalarioAEmbargar(decimal salarioBruto)
        {
            decimal salarioNeto = this.CalcularSalarioNeto(salarioBruto);
            decimal montoEmbargado = 0;
            if (salarioNeto > SALARIO_INEMBARGABLE) montoEmbargado += salarioNeto / 8;
            if (salarioNeto > (SALARIO_INEMBARGABLE * 3)) montoEmbargado += salarioNeto / 4;

            return montoEmbargado;
        }

        /* Permite realizar el calculo de los embargos al salario del empleado
         * Se da prioridad a la pension alimenticia
         * Se cubre la pension alimenticia, luego el embargo1 y el embargo2
         * Se debe cumplir con la totalidad de un embargo antes de proceder al siguiente
         * @Param SALARIO salarioEmpleado: objeto de tipo SALARIO al que se le realizará el proceso de embargo
         */
        private void ProcesarEmbargos(SALARIO salarioEmpleado)
        {
            //Calculo del monto deducible del salario
            decimal montoEmbargado = this.CalcularSalarioAEmbargar(Convert.ToDecimal(salarioEmpleado.SALARIO_BRUTO));

            //Se asigna el valor al objeto de tipo SALARIO
            salarioEmpleado.MONTO_DEDUCIBLE = montoEmbargado;

            //Se obtiene la información requerida y se asigna a una variable para facilitar su manipulación (es opcional)
            decimal pension = Convert.ToDecimal(salarioEmpleado.PENSION);
            decimal embargo1 = Convert.ToDecimal(salarioEmpleado.EMBARGO1);
            decimal embargo2 = Convert.ToDecimal(salarioEmpleado.EMBARGO2);

            //Variables locales que almacenaran la información del monto de rebajo de pensión y rebajo de embargo
            decimal rebajoPension = 0;
            decimal rebajoEmbargos = 0;

            // Embargo correspondiente a la pensión alimenticia
            if (pension > 0 && pension <= montoEmbargado) //monto deducible es suficiente
            {
                rebajoPension = pension;
                montoEmbargado -= rebajoPension;
            }
            else if (pension > 0 && pension > montoEmbargado) // monto deducible es insuficiente
            {
                rebajoPension = montoEmbargado;
                montoEmbargado = 0;
            }

            // Otros embargos del salario (embargo1 y embargo2)
            if ((embargo1 > 0 || embargo2 > 0) && montoEmbargado > 0)// Si existen embargos y aun hay monto deducible 
            {
                if (embargo1 <= montoEmbargado)//monto deducible es suficiente para embargo1
                {
                    rebajoEmbargos += embargo1;
                    montoEmbargado -= embargo1;

                    if (embargo2 <= montoEmbargado && montoEmbargado > 0) //monto deducible es suficiente para embargo2
                    {
                        rebajoEmbargos += embargo2;
                        montoEmbargado -= embargo2;
                    }
                    else if (embargo2 > montoEmbargado) //monto deducible es insuficiente para embargo2
                    {
                        rebajoEmbargos += montoEmbargado;
                        montoEmbargado = 0;
                    }

                }
                else if (embargo1 > montoEmbargado) //monto deducible es insuficiente para embargo1
                {
                    rebajoEmbargos = montoEmbargado;
                    montoEmbargado = 0;
                }

            }
            //Se asignan los valores al objeto de tipo SALARIO
            salarioEmpleado.REBAJO_PENSION = rebajoPension;
            salarioEmpleado.REBAJO_EMBARGO = rebajoEmbargos;

            //Se indica que el objeto de tipo SALARIO ha sido modificado
            db.Entry(salarioEmpleado).State = EntityState.Modified;
            //Se guardan los cambios del objeto
            db.SaveChanges();

        }

        

        public void Dispose()
        {
            ((IDisposable)db).Dispose();
        }
    }
}