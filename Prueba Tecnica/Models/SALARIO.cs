//------------------------------------------------------------------------------
// Entity data model asociado a la base de datos, corresponde a la estructura de
// tabla llamada SALARIO en la base de datos
//------------------------------------------------------------------------------

namespace Prueba_Tecnica.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class SALARIO
    {
        public int ID { get; set; }
        //Esta anotación permite cambiar del nombre con el que el usuario visualiza la columna de la tabla, afecta unicamete a la vista
        [Display(Name = "Empleado")]
        public string EMPLEADO { get; set; }
        [Display(Name = "Salario Bruto")]
        public Nullable<decimal> SALARIO_BRUTO { get; set; }
        [Display(Name = "Monto deducible")]
        public Nullable<decimal> MONTO_DEDUCIBLE { get; set; }
        [Display(Name = "Pensión")]
        public Nullable<decimal> PENSION { get; set; }
        [Display(Name = "Embargo 1")]
        public Nullable<decimal> EMBARGO1 { get; set; }
        [Display(Name = "Embargo 2")]
        public Nullable<decimal> EMBARGO2 { get; set; }
        [Display(Name = "Rebajo Pensión")]
        public Nullable<decimal> REBAJO_PENSION { get; set; }
        [Display(Name = "Rebajo Embargos")]
        public Nullable<decimal> REBAJO_EMBARGO { get; set; }
    }
}
